import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Client } from './interfaces/client.interface';
import { CreateClientDto } from './dto/client.dto';
import { UpdateClientDto } from './dto/clientUpdate.dto ';
import { CreateDirectionDto } from './dto/direction.dto';

@Injectable()
export class ClientService {
  constructor(@Inject('CLIENT_MODEL') private readonly clientModel: Model<Client>) {}

  async create(createClientDto: CreateClientDto): Promise<Client> {
    const createdCat = new this.clientModel(createClientDto);
    return await createdCat.save();
  }

  async findAll(): Promise<Client[]> {
    return await this.clientModel.find().exec();
  }

  async findBy(filter: Object): Promise<Client[]> {
    return await this.clientModel.find(filter).exec();
  }

  async update(updateClientDto: UpdateClientDto, id: string): Promise<Client> {
    //TODO findByIdAndDelete and findByIdAndUpdate deprecated
    const updateClient = this.clientModel.findByIdAndUpdate({ _id: id }, updateClientDto, { new: true });
    return await updateClient;
  }

  async delete(id: string): Promise<Client> {
    const deleteClient = this.clientModel.findByIdAndDelete({ _id: id });
    return await deleteClient;
  }

  async createDirection(createDirectionDto: CreateDirectionDto, id: string): Promise<Client> {
    const client = this.clientModel.findByIdAndUpdate({ _id: id}, {$push: { directions: createDirectionDto}}, {new: true});
    return await client;
  }

  async deleteDirection(id: string, dir_id: string): Promise<Client> {
    const client = this.clientModel.findByIdAndUpdate({ _id: id}, {$pull: { directions: {_id: dir_id}}}, {new: true});
    return await client;
  }

  async updateDirection(createDirectionDto: CreateDirectionDto, id: string, dir_id: string): Promise<Client> {
    const client = this.clientModel.findOneAndUpdate({ _id: id, "directions._id": dir_id}, {$set: {"directions.$": createDirectionDto}}, {new: true});
    return await client;
  }
}
