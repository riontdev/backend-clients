import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, HttpException, HttpStatus } from '@nestjs/common';
import { AnySchema, Schema } from '@hapi/joi';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private readonly schema: Schema) {}

  transform(value: any, metadata: ArgumentMetadata) {
    const { error } = this.schema.validate(value);
    if (error) {
        throw new HttpException({
            message: 'Validation failed',
            detail: error.message.replace(/"/g, `'`),
            statusCode: HttpStatus.BAD_REQUEST
        }, HttpStatus.BAD_REQUEST);
    }
    return value;
  }
}
