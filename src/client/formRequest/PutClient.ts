import Joi = require("@hapi/joi");

export const FormPutClient = Joi.object({
      name: Joi.string().min(3).max(20).required(),
      lastName: Joi.string().min(3).max(20).required(),
      email: Joi.string().email().required(),
      gender: Joi.string().valid('M', 'F').required(),
      rut: Joi.string().required(),
      age: Joi.number().greater(17).required(),
      phoneNumber: Joi.string().required()
});