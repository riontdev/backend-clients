import Joi = require("@hapi/joi");

export const FormClient = Joi.object({
      name: Joi.string().min(3).max(20).required(),
      lastName: Joi.string().min(3).max(20).required(),
      email: Joi.string().email().required(),
      gender: Joi.string().valid('M', 'F').required(),
      rut: Joi.string().required(),
      age: Joi.number().greater(17).required(),
      phoneNumber: Joi.string().required(),
      directions: Joi.array().items(
            Joi.object().keys(
            { 
                  province: Joi.string().required(), 
                  city: Joi.string().required(), 
                  country: Joi.string().required(), 
                  dir: Joi.string().required(), 
            }
      ))
});