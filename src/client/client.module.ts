import { Module } from '@nestjs/common';
import { ClientService } from './clients.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientSchema } from './schemas/client.schema';
import { ClientController } from './client.controller';
import { DatabaseModule } from '../database/database.module';
import { ClientProviders } from './client.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ClientController],
  providers: [ClientService,...ClientProviders]
})
export class ClientModule {}
