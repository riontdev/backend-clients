import { Document } from 'mongoose';

export interface Client extends Document {
    readonly name: string;
    readonly lastName: string;
    readonly email: string;
    readonly gender: string;
    readonly rut: string;
    readonly age: number;
    readonly phoneNumber: string;
    readonly directions: [{
        readonly province: string;
        readonly city: string;
        readonly dir: string;
        readonly country: string;
    }];
}