import Joi = require("@hapi/joi");

export class CreateDirectionDto {
    readonly province: string;
    readonly city: string;
    readonly dir: string;
    readonly country: string;
  }