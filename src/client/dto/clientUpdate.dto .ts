import Joi = require("@hapi/joi");

export class UpdateClientDto {
    readonly name: string;
    readonly lastName: string;
    readonly email: string;
    readonly gender: string;
    readonly rut: string;
    readonly age: number;
    readonly phoneNumber: string;
  }