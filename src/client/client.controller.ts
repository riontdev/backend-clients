import { Controller, Get, Put, Post, Delete, UseGuards, Request, Query, Res, Param, Body, UsePipes } from '@nestjs/common';
import { ClientService } from './clients.service';
import { CreateClientDto } from './dto/client.dto';
import { Client } from './interfaces/client.interface';
import { UpdateClientDto } from './dto/clientUpdate.dto ';
import { FormClient } from './formRequest/formClient';
import { CreateDirectionDto } from './dto/direction.dto';
import { FormPutClient } from './formRequest/PutClient';
import { JoiValidationPipe } from './joi.pipe';

@Controller('clients')
export class ClientController {
    private limitPerPage: number = 100;
    constructor(private readonly clientService: ClientService) { }

    @Get()
    async findAll():  Promise<Client[]> {
        return this.clientService.findAll();
    }

    @Get('search/rut/:rut')
    async findByRut(@Param('rut') rut: string):  Promise<Client[]> {
        return this.clientService.findBy({rut: rut});
    }

    @Get('search/name/:name')
    async findByName(@Param('name') name: string):  Promise<Client[]> {
        return this.clientService.findBy({name: name});
    }

    @Get('search/last_name/:last_name')
    async findByLastName(@Param('last_name') last_name: string):  Promise<Client[]> {
        return this.clientService.findBy({lastName: last_name});
    }

    @Get('search/gender/:gender')
    async findByGender(@Param('gender') gender: string):  Promise<Client[]> {
        return this.clientService.findBy({gender: gender});
    }

    @Get('search/email/:email')
    async findByEmail(@Param('email') email: string):  Promise<Client[]> {
        return this.clientService.findBy({email: email});
    }

    @Get('search/directions/province/:province')
    async findByDirectionProvince(@Param('province') province: string):  Promise<Client[]> {
        return this.clientService.findBy({'directions.province': province});
    }

    @Get('search/directions/country/:country')
    async findByDirectionCountry(@Param('country') country: string):  Promise<Client[]> {
        return this.clientService.findBy({'directions.country': country});
    }

    @Get('search/directions/city/:city')
    async findByDirectionCity(@Param('city') city: string):  Promise<Client[]> {
        return this.clientService.findBy({'directions.city': city});
    }

    @Get('search/directions/dir/:dir')
    async findByDirectionStreet(@Param('dir') dir: string):  Promise<Client[]> {
        return this.clientService.findBy({'directions.dir': dir});
    }

    // CREATE ALL DATA
    @Post()
    @UsePipes(new JoiValidationPipe(FormClient))
    async create(@Body() createClientDto: CreateClientDto):  Promise<any> {
        return this.clientService.create(createClientDto);
    }

    //CREATE ONLY DIRECTION
    @Post('direction/:id')
    async createDirection(@Body() createDirectionDto: CreateDirectionDto, @Param('id') id: string):  Promise<Client> {
        return this.clientService.createDirection(createDirectionDto, id);
    }

    @Delete('/:id/direction/:dir_id')
    async deleteDirection(@Param('id') id: string, @Param('dir_id') dir_id: string):  Promise<Client> {
        return this.clientService.deleteDirection(id,dir_id);
    }

    //UPDATE ALL DATA
    @Put(':id')
    async update(@Param('id') id: string, @Body() updateClientDto: UpdateClientDto):  Promise<Client> {
      return this.clientService.update(updateClientDto, id);
    }

    @Put('/:id/direction/:dir_id')
    async updateDirection(@Param('id') id: string,@Param('dir_id') dir_id: string, @Body() createDirectionDto: CreateDirectionDto):  Promise<Client> {
      return this.clientService.updateDirection(createDirectionDto,id,dir_id);
    }
  
    //DELETE ALL DATA
    @Delete(':id')
    async remove(@Param('id') id: string):  Promise<Client> {
      return this.clientService.delete(id);
    }
}
