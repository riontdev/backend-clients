import * as mongoose from 'mongoose';

export const ClientSchema = new mongoose.Schema({
  name: String,
  lastName: String,
  phoneNumber: String,
  gender: String,
  email: String,
  age: Number,
  rut: {
    type: String,
    unique: true
  },
  directions: [{
    province: String,
    country: String,
    city: String,
    dir: String
  }]
}, {
  timestamps: true
});