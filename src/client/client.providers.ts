import { Connection } from 'mongoose';
import { ClientSchema } from './schemas/client.schema';

export const ClientProviders = [
  {
    provide: 'CLIENT_MODEL',
    useFactory: (connection: Connection) => connection.model('Client', ClientSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];