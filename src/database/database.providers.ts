import * as mongoose from 'mongoose';

const uriDatabase = 'mongodb://riont:r123456@ds133338.mlab.com:33338/test_pharol';
export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(uriDatabase),
  },
];